﻿using System;
using Xunit;
using Unity;
using InfiniteSpace.Model;
using InfiniteSpace.Model.Implementation;

namespace InfiniteSpace.Model.XUnitTest {
	public static class TestHelper {
		public static IUnityContainer GetDIContainer() {
			return GetDIContainer(new Config());
		}

		public static IUnityContainer GetDIContainer(Config config) {
			IUnityContainer diContainer = new UnityContainer();
			diContainer.RegisterInstance<Config>(config)
			.RegisterSingleton<IRandomGenerator, RandomGenerator>()
			.RegisterType<IWorldObjectsManager, WorldObjectsManager>()
			.RegisterType<ISortedPlanetList, SortedPlanetList>()
			.RegisterType<IPlanetFinder, PlanetFinder>()
			.RegisterType<IRegionsScanner, RegionsScanner>()
			.RegisterType<IVisibleRegions, VisibleRegions>()
			.RegisterType<IRegion, AsyncRegion>()
			.RegisterSingleton<IPlayer, Player>()
			.RegisterSingleton<IObservedSpace, ObservedSpace>()
			.RegisterType<IObserverPlanetCollection, ObserverPlanetCollection>()
			.RegisterInstance<IFactory<IRegion>>(
				new Factory<IRegion>(() => diContainer.Resolve<IRegion>()));
			return diContainer;
		}
	}
}
