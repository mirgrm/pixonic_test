﻿using System;
using Xunit;
using Unity;
using System.Collections.Generic;
using InfiniteSpace.Model.Implementation;

namespace InfiniteSpace.Model.XUnitTest {
	public class RegionsScannerTest {

		(uint x, uint y) Convert((uint X, uint Y, uint Delta) planet) {
			return (planet.X, planet.Y);
		}

		[Fact]
		public void TestRegionsScanner() {
			var diContainer = TestHelper.GetDIContainer();
			var scanner = diContainer.Resolve<IRegionsScanner>();
			Func<ISortedPlanetList> getList = () => diContainer.Resolve<ISortedPlanetList>();

			var regions = new IRegionInfo[] {
				new TestRegionInfo(getList(),(16,16,16),(17,17,17),(18,18,18),(100,100,100)),
				new TestRegionInfo(getList(),(40,4,4)),
				new TestRegionInfo(getList(),(13,13,13),(14,14,14),(15,15,15),(120,120,120)),
				new TestRegionInfo(getList(),(19,19,19),(20,20,20),(21,21,21),(130,130,130)),
				new TestRegionInfo(getList(),(7,70,7),(8,8,8),(9,9,9),(140,140,140)),
				new TestRegionInfo(getList(),(1,1,1),(2,2,2),(0,0,0),(150,150,150)),
				new TestRegionInfo(getList(),(10,10,10),(11,11,11),(12,12,12),(72,72,72),(80,80,80),(160,160,160))
			};

			scanner.Scan(regions);
			//В начале в регионах нет планет, сканер только запросит дополнительное сканирование
			Assert.Equal(0U, scanner.BestPlanets.CountItemInArray);

			//Теперь появляется первая партия, но сканер запросит еще
			scanner.Scan(regions);
			Assert.Equal((1U, 1U), Convert(scanner.BestPlanets[0]));
			Assert.Equal((40U, 4U), Convert(scanner.BestPlanets[1]));
			Assert.Equal((7U, 70U), Convert(scanner.BestPlanets[2]));
			Assert.Equal((10U, 10U), Convert(scanner.BestPlanets[3]));
			Assert.Equal((13U, 13U), Convert(scanner.BestPlanets[4]));
			Assert.Equal((16U, 16U), Convert(scanner.BestPlanets[5]));
			Assert.Equal((19U, 19U), Convert(scanner.BestPlanets[6]));
			Assert.Equal(Constants.EmtyPlanetInfo, scanner.BestPlanets[7]);

			scanner.Scan(regions);
			Assert.Equal((1U, 1U), Convert(scanner.BestPlanets[0]));
			Assert.Equal((2U, 2U), Convert(scanner.BestPlanets[1]));
			Assert.Equal((40U, 4U), Convert(scanner.BestPlanets[2]));
			Assert.Equal((7U, 70U), Convert(scanner.BestPlanets[3]));
			Assert.Equal((8U, 8U), Convert(scanner.BestPlanets[4]));


			scanner.Scan(regions);
			Assert.Equal((0U, 0U), Convert(scanner.BestPlanets[0]));
			Assert.Equal((1U, 1U), Convert(scanner.BestPlanets[1]));
			Assert.Equal((2U, 2U), Convert(scanner.BestPlanets[2]));
			Assert.Equal((40U, 4U), Convert(scanner.BestPlanets[3]));

			Assert.False(scanner.Scanned);

			scanner.Scan(regions);
			scanner.Scan(regions);
			scanner.Scan(regions);
			scanner.Scan(regions);
			scanner.Scan(regions);
			Assert.Equal((0U, 0U), Convert(scanner.BestPlanets[0]));
			Assert.Equal((21U, 21U), Convert(scanner.BestPlanets[18]));
			Assert.Equal((72U, 72U), Convert(scanner.BestPlanets[19]));

			Assert.True(scanner.Scanned);
		}

		class TestRegionInfo : IRegionInfo {
			Queue<(uint X, uint Y, uint Delta)> _testPlanets;
			ISortedPlanetList _visiblePlanets;
			public IReadonlySortedPlanetList VisiblePlanets => _visiblePlanets;

			public Rect FullRegionRect => throw new NotImplementedException();

			public Rect ActualRect => throw new NotImplementedException();

			public bool IsFullVisible => throw new NotImplementedException();

			public void RequestMorePlanets(uint count) {
				for (var i = VisiblePlanets.CountItemInArray; i < count; i++) {
					if (_testPlanets.Count > 0) {
						_visiblePlanets.TryEnqueueAndIcnrement(_testPlanets.Dequeue());
					} else {
						_visiblePlanets.TryEnqueueAndIcnrement(Constants.ErrorPlanet);
					}
				}
			}

			public TestRegionInfo(ISortedPlanetList sortedPlanetList, params (uint X, uint Y, uint Delta)[] planets) {
				_visiblePlanets = sortedPlanetList;
				_testPlanets = new Queue<(uint X, uint Y, uint Delta)>(planets);
			}

			public override string ToString() {
				return $"{VisiblePlanets[0].Delta},{VisiblePlanets[1].Delta},{VisiblePlanets[2].Delta},{VisiblePlanets[3].Delta}";
			}
		}
	}
}
