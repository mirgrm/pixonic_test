﻿using System;
using Xunit;
using Unity;


namespace InfiniteSpace.Model.XUnitTest {
	public class WorldObjectsManagerTest {

		[Fact]
		public void TestWorlObjectsManager() {
			var diContainer = TestHelper.GetDIContainer();
			var worldObjectsManager = diContainer.Resolve<IWorldObjectsManager>();

			var stopwatch = System.Diagnostics.Stopwatch.StartNew();
			var planetsCount = 0U;
			var Size = 1000U;
			for (var x=0U-Size/2; x!=Size/2; x++) {
				for (var y = 0U-Size / 2; y!=Size/2; y++) {
					var isPlanet = worldObjectsManager.HasPlanet(x, y);
					if (isPlanet) {
						planetsCount++;
						var rating = worldObjectsManager.GetPlanetRating(x, y);
					}
				}
			}
			var workTime = stopwatch.ElapsedMilliseconds;
			stopwatch.Stop();
			//Проверяем скорость работы генератора
			Assert.True(workTime<5*1000);


			var realProbability = planetsCount * 100f / (Size * Size);
			var targetProbability = diContainer.Resolve<Implementation.Config>().PlanetsProbabilityPercents;
			//Проверяем точность случайного распределения объектов
			Assert.Equal(targetProbability, realProbability, 1);

		}
	}
}
