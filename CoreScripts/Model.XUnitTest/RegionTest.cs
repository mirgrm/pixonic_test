﻿using System;
using Xunit;
using Unity;
using System.Collections.Generic;
using InfiniteSpace.Model.Implementation;


namespace InfiniteSpace.Model.XUnitTest {
	public class RegionTest {

		[Fact]
		public async System.Threading.Tasks.Task TestRegion() {
			var diContainer = TestHelper.GetDIContainer();
			var regionFactory = diContainer.Resolve<IFactory<IRegion>>();
			var region = regionFactory.CreateNew();
			region.SetIsFullVisible(false);
			region.SetActualRect(new Rect(0, 0, 1000, 1000));
			region.RequestMorePlanets(5);
			await System.Threading.Tasks.Task.Delay(100);
			Assert.Equal(5U, region.VisiblePlanets.CountItemInArray);

			region.SetIsFullVisible(true);
			region.SetFullRegionRect(new Rect(0, 0, 4048, 4048));
			region.RequestMorePlanets(10);
			await System.Threading.Tasks.Task.Delay(100);
			region.RequestMorePlanets(10);
			await System.Threading.Tasks.Task.Delay(100);
			Assert.Equal(10U, region.VisiblePlanets.CountItemInArray);
		}
	}
}
