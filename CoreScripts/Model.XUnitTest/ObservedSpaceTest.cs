﻿using System;
using Xunit;
using Unity;
using InfiniteSpace.Model.Implementation;
using System.Threading.Tasks;

namespace InfiniteSpace.Model.XUnitTest {
	public class ObservedSpaceTest {

		[Fact]
		public async Task TestObservedSpace() {
			var diContainer = TestHelper.GetDIContainer();
			var config = diContainer.Resolve<Config>();
			var observedSpace = diContainer.Resolve<IObservedSpace>();
			observedSpace.Frame = new Rect(0, 0, 1000, 1000);
			var planets = observedSpace.BestPlanets;
			await Task.Delay(50);
			observedSpace.Update();
			await Task.Delay(50);
			observedSpace.Update();
			await Task.Delay(50);
			observedSpace.Update();
			await Task.Delay(50);
			observedSpace.Update();
			Assert.Equal(config.TopPlanetsCount, observedSpace.BestPlanets.CountItemInArray);

			observedSpace.Frame = new Rect(5, 5, 10, 10);
			for (var i = 0; i < 3; i++) {
				await Task.Delay(50);
				observedSpace.Update();
			}

			observedSpace.Frame = new Rect(5, 5, 10, 10002);
			for (var i = 0; i < 5; i++) {
				await Task.Delay(50);
				observedSpace.Update();
			}
			Assert.Equal(config.TopPlanetsCount, observedSpace.BestPlanets.CountItemInArray);
		}

		[Fact]
		public async Task TestObservedSpaceBig() {
			var diContainer = TestHelper.GetDIContainer();
			var config = diContainer.Resolve<Config>();
			var observedSpace = diContainer.Resolve<IObservedSpace>();
			observedSpace.Frame = new Rect(0, 0, 1000, 1000);
			var planets = observedSpace.BestPlanets;
			while (observedSpace.ScanWaiting) {
				observedSpace.Update();
				await Task.Delay(50);
			}
			Assert.Equal(config.TopPlanetsCount, observedSpace.BestPlanets.CountItemInArray);
		}

		[Fact]
		public async Task TestObservedSpaceMini() {
			var diContainer = TestHelper.GetDIContainer();
			var config = diContainer.Resolve<Config>();
			var observedSpace = diContainer.Resolve<IObservedSpace>();
			observedSpace.Frame = new Rect(501, 501, 501, 501);
			var planets = observedSpace.BestPlanets;
			while (observedSpace.ScanWaiting) {
				observedSpace.Update();
				await Task.Delay(10);
			}
			Assert.Equal(Constants.ErrorPlanet, observedSpace.BestPlanets[observedSpace.BestPlanets.CountItemInArray-1]);
		}

		[Fact]
		public async Task TestObservedSpaceOverflow() {
			var diContainer = TestHelper.GetDIContainer();
			var config = diContainer.Resolve<Config>();
			var observedSpace = diContainer.Resolve<IObservedSpace>();
			observedSpace.Frame = new Rect(uint.MaxValue - 200, uint.MaxValue - 300, 100, 200);
			var planets = observedSpace.BestPlanets;
			while (observedSpace.ScanWaiting) {
				observedSpace.Update();
				await Task.Delay(10);
			}
			Assert.Equal(config.TopPlanetsCount, observedSpace.BestPlanets.CountItemInArray);
		}
	}
}
