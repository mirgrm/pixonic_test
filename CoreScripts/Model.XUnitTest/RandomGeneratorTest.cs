﻿using System;
using Xunit;
using Unity;

namespace InfiniteSpace.Model.XUnitTest {
	public class RandomGeneratorTest {
		[Fact]
		public void TestRandomGenerator() {
			var diContainer = TestHelper.GetDIContainer();
			var random = diContainer.Resolve<IRandomGenerator>();

			//Проверка детерминированности генератора
			Assert.Equal(random.GetRandom(235235, 10), random.GetRandom(235235, 10));

			var planets = 0;
			var countCells = 1000000U;
			var targetProbability = 30;
			for (var i=0U; i< countCells; i++) {
				if (random.GetRandom(i, 100U) < targetProbability) planets++;
			}
			var realProbability = planets * 100f / countCells;

			//Проверяем точность случайного распределения объектов
			Assert.Equal(targetProbability, realProbability, 2);
		}


	}
}
