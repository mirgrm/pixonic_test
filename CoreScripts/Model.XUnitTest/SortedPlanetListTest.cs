﻿using System;
using Xunit;
using Unity;
using InfiniteSpace.Model.Implementation;

namespace InfiniteSpace.Model.XUnitTest {
	public class SortedPlanetListTest {

		[Fact]
		public void TestSortedPlanetList() {
			var diContainer = TestHelper.GetDIContainer();
			var list = diContainer.Resolve<ISortedPlanetList>();
			list.CountItemInArray = 5;
			list
				.ForceEnqueue((0, 0, 5))
				.ForceEnqueue((1, 1, 4))
				.ForceEnqueue((2, 2, 3))
				.ForceEnqueue((3, 3, 10))
				.ForceEnqueue((4, 3, 16))
				.ForceEnqueue((2, 1, 2));

			Assert.Equal((2U, 1U, 2U), list[0]);
			Assert.Equal((2U, 2U, 3U), list[1]);
			Assert.Equal((1U, 1U, 4U), list[2]);
			Assert.Equal((0U, 0U, 5U), list[3]);
			Assert.Equal((3U, 3U, 10U), list[4]);
			Assert.Equal(Constants.ErrorDelta, list[5].Delta);

			list.ForceEnqueue((5U, 100U, 1U));

			Assert.Equal((5U, 100U, 1U), list[0]);
			Assert.Equal((2U, 1U, 2U), list[1]);
			Assert.Equal((2U, 2U, 3U), list[2]);
			Assert.Equal((1U, 1U, 4U), list[3]);
			Assert.Equal((0U, 0U, 5U), list[4]);
			Assert.Equal(Constants.ErrorDelta, list[5].Delta);
			Assert.Equal(Constants.ErrorDelta, list[5].Delta);
			Assert.True(list.Contains((2, 2, 3)));


			list.CountItemInArray = 6;
			list.ForceEnqueue((21515, 23525, 1254));
			Assert.Equal((21515U, 23525U, 1254U), list[5]);
			list.ForceEnqueue((6585, 47, 0));
			Assert.Equal((0U, 0U, 5U), list[5]);

			list.TryEnqueue((44, 4, 4000));
			Assert.Equal((0U, 0U, 5U), list[5]);
			list.ForceEnqueue((44, 4, 4000));
			Assert.Equal((44U, 4U, 4000U), list[5]);

			Assert.True(list.Contains((44, 4, 4000)));
		}


		[Fact]
		public void TestSortedPlanetListTryAddAndIncrement() {
			var diContainer = TestHelper.GetDIContainer();
			var list = diContainer.Resolve<ISortedPlanetList>();
			for (uint i = 10; i < 15; i++) {
				list.TryEnqueueAndIcnrement((i, i, i));
			}
			for (uint i = 0; i < 10; i++) {
				list.TryEnqueueAndIcnrement((i, i, i));
			}
			for (uint i = 15; i < 30; i++) {
				list.TryEnqueueAndIcnrement((i, i, i));
			}
			Assert.Equal((0U, 0U, 0U), list[0]);
			Assert.Equal((5U, 5U, 5U), list[5]);
			Assert.Equal((19U, 19U, 19U), list[19]);
			Assert.False(list.Contains((20, 20, 20)));
			for (uint i = 0; i < 20; i++) {
				Assert.True(list.Contains((i, i, i)));
			}
			for (uint i = 20; i < 30; i++) {
				Assert.False(list.Contains((i, i, i)));
			}
		}

		[Fact]
		public void TestSortedPlanetListTryAddAndIncrement2() {
			var diContainer = TestHelper.GetDIContainer();
			var list = diContainer.Resolve<ISortedPlanetList>();
			for (uint i = 15; i < 30; i++) {
				list.TryEnqueueAndIcnrement((i, i, i));
			}
			for (uint i = 10; i < 15; i++) {
				list.TryEnqueueAndIcnrement((i, i, i));
			}
			for (uint i = 0; i < 10; i++) {
				list.TryEnqueueAndIcnrement((i, i, i));
			}

			Assert.Equal((0U, 0U, 0U), list[0]);
			Assert.Equal((5U, 5U, 5U), list[5]);
			Assert.Equal((19U, 19U, 19U), list[19]);
			Assert.False(list.Contains((20, 20, 20)));
			for (uint i = 0; i < 20; i++) {
				Assert.True(list.Contains((i, i, i)));
			}
			for (uint i = 20; i < 30; i++) {
				Assert.False(list.Contains((i, i, i)));
			}
		}
	}
}
