﻿using System;
using Xunit;
using Unity;
using InfiniteSpace.Model.Implementation;

namespace InfiniteSpace.Model.XUnitTest {
	public class PlanetFinderTest {

		class TestPlayer : IPlayer {
			public uint Rating { get; }

			public Point Point => throw new NotImplementedException();

			Point IPlayer.Point { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

			public TestPlayer(uint rating) {
				Rating = rating;
			}

			public uint CalcDelta(uint x, uint y, uint planetRating) {
				var dRating = Math.Min(Rating - planetRating, planetRating - Rating);
				return dRating;
			}
		}

		IPlanetFinder GetPlanetFinder() {
			var diContainer = TestHelper.GetDIContainer();
			diContainer.RegisterInstance<IWorldObjectsManager>(
				new MemoryWorldObjectsManager()
				.AddPlanet(10, 10, 10)
				.AddPlanet(10, 15, 11)
				.AddPlanet(15, 10, 12)
				.AddPlanet(25, 20, 13)
				.AddPlanet(30, 30, 14)
				.AddPlanet(0, 90, 15)
				.AddPlanet(0, 95, 15)
				.AddPlanet(30, 40, 16)
				.AddPlanet(0, 0, 17)
				.AddPlanet(1, 1, 20)
				.AddPlanet(1, 2, 25)
			);

			return diContainer.Resolve<IPlanetFinder>();
		}

		[Fact]
		public void SearchBestPlanet() {
			var f = GetPlanetFinder();

			//Поиск в пустом пространстве
			var rect0 = new Rect(100000, 100000, 100005, 100005);
			Assert.Equal(Constants.ErrorDelta, f.SearchBestPlanet(rect0, new Player(10), 0).Delta);

			//Обычный поиск лучшей планеты
			var rect = new Rect(uint.MaxValue - 5, 0, 100, 100);
			Assert.Equal((10U, 15U, 0U), f.SearchBestPlanet(rect, new TestPlayer(11), 0));
			Assert.Equal((10U, 10U, 5U), f.SearchBestPlanet(rect, new TestPlayer(5), 0));
			Assert.Equal((1U, 2U, 25U), f.SearchBestPlanet(rect, new TestPlayer(50), 0));
			Assert.Equal((1U, 2U, 1U), f.SearchBestPlanet(rect, new TestPlayer(24), 0));
			Assert.Equal((1U, 1U, 1U), f.SearchBestPlanet(rect, new TestPlayer(21), 0));
			Assert.Equal((0U, 0U, 0U), f.SearchBestPlanet(rect, new TestPlayer(17), 0));
			Assert.Equal((0U, 0U, 1U), f.SearchBestPlanet(rect, new TestPlayer(18), 0));
		}

		[Fact]
		public void SearchSecondBestPlanets() {
			var f = GetPlanetFinder();

			var rect = new Rect(0, 0, 100U, 100U);

			//Ищем планеты на втором и третьем месте
			Assert.Equal((30U, 40U, 1U), f.SearchBestPlanet(rect, new TestPlayer(17), 1));

			Assert.Equal((1U, 2U, 25U), f.SearchBestPlanet(rect, new TestPlayer(50), 0));
			Assert.Equal((1U, 1U, 30U), f.SearchBestPlanet(rect, new TestPlayer(50), 1));

			//Когда есть повторение
			var pl0 = f.SearchBestPlanet(rect, new TestPlayer(15), 0);
			var pl1 = f.SearchBestPlanet(rect, new TestPlayer(15), 1);
			Assert.True(pl0 == (0U, 90U, 0U) && pl1 == (0U, 95U, 0U) 
				|| pl1 == (0U, 90U, 0U) && pl0 == (0U, 95U, 0U));

			var rect2 = new Rect(0, 0, 10, 10);
			//(10, 10, 10),(0, 0, 17),(1, 1, 20),(1, 2, 25) 
			Assert.Equal((10U, 10U, 0U), f.SearchBestPlanet(rect2, new TestPlayer(10), 0));
			Assert.Equal((0U, 0U, 7U), f.SearchBestPlanet(rect2, new TestPlayer(10), 1));
			Assert.Equal((1U, 1U, 10U), f.SearchBestPlanet(rect2, new TestPlayer(10), 2));
			Assert.Equal((1U, 2U, 15U), f.SearchBestPlanet(rect2, new TestPlayer(10), 3));

			//Больше нет планет
			Assert.Equal(Constants.ErrorDelta, f.SearchBestPlanet(rect2, new TestPlayer(10), 4).Delta);
		}
	}
}
