﻿using System;
using Xunit;
using Unity;
using InfiniteSpace.Model.Implementation;
using System.Linq;

namespace InfiniteSpace.Model.XUnitTest {
	public class VisibleRegionsTest {
		[Fact]
		public void TestVisibleFullRegions() {
			var diContainer = TestHelper.GetDIContainer();
			var regionsViewer = diContainer.Resolve<IVisibleRegions>();
			var config = diContainer.Resolve<Config>();
			var regionSize = 1U << config.RegionBitSize;

			//Тестим один регион
			var rect1x1 = new Rect(
				0, 0, regionSize - 1, regionSize - 1);
			var regions = regionsViewer.GetVisibleRegions(rect1x1);
			Assert.Single(regions);
			Assert.True(regions.First().IsFullVisible);

			//Тестим неколько регионов
			var rect2x2 = new Rect(
				0, 0, regionSize * 2 - 1, regionSize * 2 - 1);
			Assert.Equal(4, regionsViewer.GetVisibleRegions(rect2x2).Count());
			var rect3x2 = new Rect(
				0, 0, regionSize * 3 - 1, regionSize * 2 - 1);
			Assert.Equal(6, regionsViewer.GetVisibleRegions(rect3x2).Count());
			var rect2x3 = new Rect(
				0, 0, regionSize * 2 - 1, regionSize * 3 - 1);
			regions = regionsViewer.GetVisibleRegions(rect2x3);
			Assert.Equal(6, regions.Count());
			Assert.True(regions.First().IsFullVisible);

			//Тестим замыкание
			var rect = new Rect(
				uint.MaxValue - regionSize + 1,
				uint.MaxValue - regionSize + 1,
				regionSize - 1,
				regionSize - 1);
			regions = regionsViewer.GetVisibleRegions(rect);
			Assert.Equal(4, regions.Count());
			Assert.True(regions.First().IsFullVisible);
			Assert.True(regions.Last().IsFullVisible);

			//Тестим центральный регион из 9х9 области
			var rect9x9 = new Rect(0, 0, regionSize * 3 - 1, regionSize * 3 - 1);
			var centerRegion = regionsViewer.GetVisibleRegions(rect9x9).ToList()[4];
			Assert.True(centerRegion.IsFullVisible);
			Assert.Equal(new Rect(regionSize, regionSize, regionSize * 2 - 1, regionSize * 2 - 1), centerRegion.ActualRect);
		}

		[Fact]
		public void TestVisibleRegions() {
			var diContainer = TestHelper.GetDIContainer();
			var regionsViewer = diContainer.Resolve<IVisibleRegions>();
			var config = diContainer.Resolve<Config>();
			var regionSize = 1U << config.RegionBitSize;

			var rect1x1 = new Rect(10, 0, regionSize - 1, regionSize - 1);
			var region = regionsViewer.GetVisibleRegions(rect1x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(rect1x1, region.ActualRect);

			rect1x1 = new Rect(0, 10, regionSize - 1, regionSize - 1);
			region = regionsViewer.GetVisibleRegions(rect1x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(rect1x1, region.ActualRect);

			rect1x1 = new Rect(0, 0, regionSize - 10, regionSize - 1);
			region = regionsViewer.GetVisibleRegions(rect1x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(rect1x1, region.ActualRect);

			rect1x1 = new Rect(0, 0, regionSize - 1, regionSize - 10);
			region = regionsViewer.GetVisibleRegions(rect1x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(rect1x1, region.ActualRect);

			rect1x1 = new Rect(10, 10, regionSize - 10, regionSize - 10);
			region = regionsViewer.GetVisibleRegions(rect1x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(rect1x1, region.ActualRect);

			var rect2x1 = new Rect(10, 0, regionSize * 2 - 1, regionSize - 1);
			region = regionsViewer.GetVisibleRegions(rect2x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(new Rect(10, 0, regionSize - 1, regionSize - 1), region.ActualRect);

			rect2x1 = new Rect(0, 10, regionSize * 2 - 1, regionSize - 1);
			region = regionsViewer.GetVisibleRegions(rect2x1).First();
			Assert.False(region.IsFullVisible);
			Assert.Equal(new Rect(0, 10, regionSize - 1, regionSize - 1), region.ActualRect);

			rect2x1 = new Rect(0, 0, regionSize * 2 - 10, regionSize - 10);
			region = regionsViewer.GetVisibleRegions(rect2x1).Last();
			Assert.False(region.IsFullVisible);
			Assert.Equal(new Rect(regionSize, 0, regionSize * 2 - 10, regionSize - 10), region.ActualRect);

			//Тестим центральный регион из 9х9 области
			var rect9x9 = new Rect(5, 6, regionSize * 3 - 7, regionSize * 3 - 8);
			var centerRegion = regionsViewer.GetVisibleRegions(rect9x9).ToList()[4];
			Assert.True(centerRegion.IsFullVisible);
			Assert.Equal(new Rect(regionSize, regionSize, regionSize * 2 - 1, regionSize * 2 - 1), centerRegion.ActualRect);
		}
	}
}
