﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;

namespace InfiniteSpace.Tests {
	public class ShowPlanetsTest {

		[UnityTest]
		public IEnumerator BigSpaceTest() {
			var observedSpace = GetObservedSpace();
			observedSpace.Frame = new Model.Rect(10000, 10000) - new Model.Point(5000, 5000);
			yield return UpdateObserved(observedSpace);
		}

		[UnityTest]
		public IEnumerator MiniSpaceTest() {
			var observedSpace = GetObservedSpace();
			observedSpace.Frame = new Model.Rect(0, 0, 21, 21);
			yield return UpdateObserved(observedSpace);
		}


		[UnityTest]
		public IEnumerator MovingSpaceTest() {
			var observedSpace = GetObservedSpace();

			observedSpace.Frame = new Model.Rect(500, 500);
			yield return UpdateObserved(observedSpace);

			var set1 = new HashSet<(uint X, uint Y, uint Delta)>();
			var set2 = new HashSet<(uint X, uint Y, uint Delta)>();
			Debug.Log($"========Moving============Start");
			for (uint offset=0; offset<1000; offset++) {
				observedSpace.Frame += new Model.Point(1, 0); 
				observedSpace.Update();
				Debug.Log($"Move {offset}");
				for (uint i = 0; i < observedSpace.BestPlanets.CountItemInArray; i++) {
					var planet = observedSpace.BestPlanets[i];
					if (!set1.Contains(planet)) {
						//set1.Add(planet);
						Debug.Log($"{offset}) New planet {planet.X} {planet.Y} {planet.Delta}");
					}
					set2.Add(planet);
				}
				foreach (var planet in set1) {
					if (!set2.Contains(planet)) {
						Debug.Log($"{offset}) Delete planet {planet.X} {planet.Y} {planet.Delta}");
					}
				}
				var temp = set1;
				set1 = set2;
				set2 = temp;
				set2.Clear();
				yield return null;
			}
			Debug.Log($"========Moving============ScanWaiting");
			yield return UpdateObserved(observedSpace);
			Debug.Log($"========Moving============Stop");

			
			// Use the Assert class to test conditions.
			// Use yield to skip a frame.

		}

		Model.IObservedSpace GetObservedSpace() {
			var configPath = "GameConfig";
			var configAsset = Resources.Load<TextAsset>(configPath);
			var config = JsonUtility.FromJson<Model.Implementation.Config>(configAsset.text);
			var container = new DiContainer();
			TestInstaller.Install(container, config);
			var observedSpace = container.Resolve<Model.IObservedSpace>();
			return observedSpace;
		}

		IEnumerator UpdateObserved(Model.IObservedSpace observedSpace) {
			while (observedSpace.ScanWaiting) {
				observedSpace.Update();
				for (uint i = 0; i < observedSpace.BestPlanets.CountItemInArray; i++) {
					var planet = observedSpace.BestPlanets[i];
					Debug.Log($"{i}) Planet {planet.X} {planet.Y} {planet.Delta}");
				}
				Debug.Log($"===================================");
				yield return null;
			}
		}


	}
}
