﻿using InfiniteSpace.Model;
using InfiniteSpace.Model.Implementation;
using UnityEngine;
using Zenject;

namespace InfiniteSpace {
	public class TestInstaller : Installer<Config, TestInstaller> {
		private Config _config;

		public TestInstaller(Config config) {
			_config = config;
		}

		public override void InstallBindings() {
			Container.RegisterInstance<Config>(_config)
			.RegisterSingleton<IRandomGenerator, RandomGenerator>()
			.RegisterType<IWorldObjectsManager, WorldObjectsManager>()
			.RegisterType<ISortedPlanetList, SortedPlanetList>()
			.RegisterType<IPlanetFinder, PlanetFinder>()
			.RegisterType<IRegionsScanner, RegionsScanner>()
			.RegisterType<IVisibleRegions, VisibleRegions>()
			.RegisterType<IRegion, Region>()
			.RegisterSingleton<IPlayer, Player>()
			.RegisterSingleton<IObservedSpace, ObservedSpace>()
			.RegisterInstance<Model.IFactory<IRegion>>(
				new Model.Implementation.Factory<IRegion>(() =>
					Container.Resolve<IRegion>()));

		}
	}


}
