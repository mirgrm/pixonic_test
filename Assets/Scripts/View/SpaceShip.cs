﻿using UnityEngine;
using UnityEngine.UI;

namespace InfiniteSpace.View {
	public class SpaceShip : MonoBehaviour {
		[SerializeField] Text _playerRatingText;
		[Zenject.Inject] Model.IPlayer _player;
		void Start() {
			_playerRatingText.text = _player.Rating.ToString();
		}
	}
}
