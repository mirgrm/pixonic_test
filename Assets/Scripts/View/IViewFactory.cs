﻿using UnityEngine;

namespace InfiniteSpace.View {
	public interface IViewFactory<T> where T : MonoBehaviour {

		T Instantiate();
		T Instantiate(Transform parentTransform);
		T Instantiate(Vector3 poistion, Quaternion rotation, Transform parentTransform);
	}
}
