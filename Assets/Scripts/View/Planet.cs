﻿using InfiniteSpace.Model;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InfiniteSpace.View {
	public class Planet : MonoBehaviour {

		[SerializeField] Text _nameText;
		[SerializeField] uint _x;
		[SerializeField] uint _y;
		[SerializeField] RectTransform _scaling;
		[Inject] IWorldObjectsManager _world;
		[Inject] IObservedSpace _observedSpace;
		private RectTransform _rectTransform;

		public Point Point {
			get => new Point(_x, _y);
			set {
				_x = value.X;
				_y = value.Y;
				PointChanged();
			}
		}

		void PointChanged() {
			_nameText.text = _world.GetPlanetRating(_x, _y).ToString();
			gameObject.name = _nameText.text;
			Update();
		}

		private void Awake() {
			_rectTransform = (transform as RectTransform);
		}

		private void Update() {
			var frame = _observedSpace.Frame;
			var sizeX = frame.SizeX;
			var sizeY = frame.SizeY;
			var panel = (transform.parent as RectTransform);
			var panelSize = panel.rect.size;

			var step = new Vector2(panelSize.x / sizeX, panelSize.y / sizeY);

			var cX = (int)(_x - frame.MinX) - (int)sizeX/2;
			var cY = (int)(_y - frame.MinY) - (int)sizeY/2;

			var newX = step.x * cX ;
			var newY = step.y * cY;
			_rectTransform.anchoredPosition = new Vector2(newX, newY);

			var objectSize = _rectTransform.rect.size;
			var objectScale = Mathf.Max(1f, Mathf.Min(panelSize.x / objectSize.x / sizeX, panelSize.y / objectSize.y / sizeY));

			_scaling.localScale = Vector3.one * objectScale;

			Debug.Log($"{sizeX} {sizeY}");
		}
	}
}
