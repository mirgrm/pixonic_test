﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteSpace {

	[CreateAssetMenu(menuName = "InfiniteSpace/ResourcesRoster")]
	public class ResourcesRoster : ScriptableObject {
		public View.SpaceShip SpaceShip;
		public View.Planet Planet;
		public View.ViewRect ViewRect;
	}
}
