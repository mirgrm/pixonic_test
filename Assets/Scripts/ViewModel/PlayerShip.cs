﻿using System.Collections.Generic;
using System.Linq;
using InfiniteSpace.Model;
using InfiniteSpace.View;

namespace InfiniteSpace.ViewModel {
	public class PlayerShip : IPlayerShip {
		public PlayerShip(IViewFactory<View.SpaceShip> spaceShipsViewFactory, IViewRoot viewRoot) {
			spaceShipsViewFactory.Instantiate(viewRoot.Transform);
		}
	}

	public interface IPlayerShip {

	}
}
