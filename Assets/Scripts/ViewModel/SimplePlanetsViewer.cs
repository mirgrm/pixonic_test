﻿using InfiniteSpace.Model;
using System.Collections.Generic;

namespace InfiniteSpace.ViewModel {
	public class SimplePlanetsViewer : ISimplePlanetsViewer {
		private IWorldObjectsManager _world;

		public IEnumerable<(uint X, uint Y)> GetAllPlanetsInViewRect(Model.Rect rect) {
			var countX = rect.SizeX;
			var countY = rect.SizeY;

			for (var xi = 0U; xi < countX; xi++) {
				for (var yi = 0U; yi < countY; yi++) {
					var x = xi + rect.MinX;
					var y = yi + rect.MinY;
					if (_world.HasPlanet(x, y)) {
						yield return (x, y);
					}
				}
			}
		}

		public SimplePlanetsViewer(IWorldObjectsManager world, IPlanetFinder planetFinder) {
			_world = world;
		}
	}

	public interface ISimplePlanetsViewer {
		IEnumerable<(uint X, uint Y)> GetAllPlanetsInViewRect(Model.Rect rect);
	}
}
