﻿using UnityEngine;
using InfiniteSpace.Model;

namespace InfiniteSpace.ViewModel {
	public class PlayerInputController : IPlayerInputController {
		private IPlayer _player;
		private IObservedSpace _observedSpace;
		private GameConfig _config;

		public uint Scale { get; private set; } = 4;

		bool GetKey(KeyCode code) {
			if (Scale < _config.ChangeModeScale) {
				return Input.GetKeyDown(code);
			} else {
				return Input.GetKey(code);
			}
		}


		public void Update() {
			var dy = (GetKey(KeyCode.W)) ? 1U : (GetKey(KeyCode.S)) ? uint.MaxValue : 0U;
			var dx = (GetKey(KeyCode.D)) ? 1U : (GetKey(KeyCode.A)) ? uint.MaxValue : 0U;
			_player.Point = new Point(_player.Point.X + dx, _player.Point.Y + dy);
			var dScale = (GetKey(KeyCode.Minus) || GetKey(KeyCode.KeypadMinus)) ? 1U
				: ((GetKey(KeyCode.Equals) || GetKey(KeyCode.KeypadPlus)) ? uint.MaxValue : 0U);
			Scale = System.Math.Min(_config.MaxScale, System.Math.Max(_config.MinScale, Scale + dScale));
			_observedSpace.Frame = new Model.Rect(
				_player.Point.X - Scale,
				_player.Point.Y - Scale,
				_player.Point.X + Scale,
				_player.Point.Y + Scale);
		}

		public PlayerInputController(IPlayer player, IObservedSpace observedSpace, GameConfig config) {
			_player = player;
			_observedSpace = observedSpace;
			_config = config;
		}

	}

	public interface IPlayerInputController {
		uint Scale { get; }
		void Update();
	}
}
