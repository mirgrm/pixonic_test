﻿using InfiniteSpace.View;
using UnityEngine;

namespace InfiniteSpace.ViewModel {
	public class ViewRoot : IViewRoot {
		public Transform Transform { get; }
		public ViewRoot(IViewFactory<ViewRect> viewRectFactory) {
			Transform = viewRectFactory.Instantiate().transform;
		}
		public void Dispose() {
			if (Transform) GameObject.Destroy(Transform);
		}
	}

	public interface IViewRoot: System.IDisposable {
		Transform Transform { get; }
	}
}
