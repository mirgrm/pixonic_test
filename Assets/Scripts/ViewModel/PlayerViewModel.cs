﻿using InfiniteSpace.Model;
using InfiniteSpace.View;

namespace InfiniteSpace.ViewModel {
	public class PlayerViewModel : IPlayerViewModel {
		private IPlanetsPool _planetsPool;
		private IObservedSpace _observedSpace;
		private IObserverPlanetCollection _observer;
		private IPlayerInputController _inputController;
		private IPlayerShip _playerShip;
		private IViewRoot _root;
		private GameConfig _config;
		private ISimplePlanetsViewer _simplePlanetsViewer;
		private bool _isDisposed = false;

		public PlayerViewModel(
			IPlanetsPool planetsPool,
			IObservedSpace observedSpaceModel,
			IObserverPlanetCollection observer,
			IPlayerInputController inputController,
			IPlayerShip playerShip,
			IViewRoot root,
			GameConfig config,
			ISimplePlanetsViewer simplePlanetsViewer
		) {
			_planetsPool = planetsPool;
			_observedSpace = observedSpaceModel;
			_observer = observer;
			_observer.AddingPlanet += _observer_AddingPlanet;
			_observer.DeletingPlanet += _observer_DeletingPlanet;
			_inputController = inputController;
			_playerShip = playerShip;
			_root = root;
			_config = config;
			_simplePlanetsViewer = simplePlanetsViewer;
		}

		private void _observer_DeletingPlanet((uint X, uint Y) planet) {
			_planetsPool.HidePlanet(new Point(planet.X, planet.Y));
		}

		private void _observer_AddingPlanet((uint X, uint Y) planet) {
			_planetsPool.ShowPlanet(new Point(planet.X, planet.Y));
		}

		public void Update() {
			if (_isDisposed) throw new System.ObjectDisposedException("PlayerViewModel is disposed");
			if (_inputController.Scale > _config.ChangeModeScale) {
				_observedSpace.Update();
				if (!_observedSpace.ScanWaiting) {
					_observer.Update(_observedSpace.BestPlanets);
				}
			} else {
				_observer.Update(_simplePlanetsViewer.GetAllPlanetsInViewRect(_observedSpace.Frame));
			}
			_inputController.Update();
		}

		public void Dispose() {
			if (_isDisposed) throw new System.ObjectDisposedException("PlayerViewModel is disposed");
			_root.Dispose();
		}
	}

	public interface IPlayerViewModel : System.IDisposable {
		void Update();
	}


}
