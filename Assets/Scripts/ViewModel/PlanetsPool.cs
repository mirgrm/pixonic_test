﻿using System.Collections.Generic;
using System.Linq;
using InfiniteSpace.Model;
using InfiniteSpace.View;

namespace InfiniteSpace.ViewModel {
	public class PlanetsPool : IPlanetsPool {
		List<Planet> _poolList = new List<Planet>();
		private IViewFactory<Planet> _planetFactory;
		private IViewRoot _viewRoot;

		public PlanetsPool(IViewFactory<Planet> planetFactory, IViewRoot viewRoot) {
			_planetFactory = planetFactory;
			_viewRoot = viewRoot;
		}

		public void ShowPlanet(Point point) {
			var oldPlanet = _poolList.FirstOrDefault(p => p.Point.X == point.X && p.Point.Y == point.Y);
			if (oldPlanet != null) {
				oldPlanet.Point = point;
				oldPlanet.gameObject.SetActive(true);
				return;
			}
			var freePlanet = _poolList.FirstOrDefault(p => !p.gameObject.activeSelf);
			if (freePlanet != null) {
				freePlanet.Point = point;
				freePlanet.gameObject.SetActive(true);
				return;
			} else {
				var newPlanet = _planetFactory.Instantiate(_viewRoot.Transform);
				newPlanet.Point = point;
				_poolList.Add(newPlanet);
				return;
			}
		}

		public void HidePlanet(Point point) {
			var oldPlanet = _poolList.FirstOrDefault(p => p.Point.X == point.X && p.Point.Y == point.Y);
			if (oldPlanet != null) {
				oldPlanet.Point = point;
				oldPlanet.gameObject.SetActive(false);
				return;
			}
		}
	}

	public interface IPlanetsPool {
		void ShowPlanet(Point point);

		void HidePlanet(Point point);
	}
}
