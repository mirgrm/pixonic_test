﻿using InfiniteSpace.ViewModel;
using UnityEngine;
using Zenject;

namespace InfiniteSpace {
	public class EntryPoint : MonoBehaviour {

		
		[SerializeField] [ResourceLink(typeof(TextAsset))] string _configPath = "GameConfig";
		[SerializeField] [ResourceLink(typeof(ResourcesRoster))] string _resourcesRosterPath = "ResourcesRoster";
		[SerializeField] RectTransform _rootTransorm;

		private IPlayerViewModel _playerController;



		private void Awake() {

			var config = Newtonsoft.Json.JsonConvert.DeserializeObject<GameConfig>(Resources.Load<TextAsset>(_configPath).text);
			var resourcesRoster = Resources.Load<ResourcesRoster>(_resourcesRosterPath);

			var gameContainer = new DiContainer();
			GameInstaller.Install(gameContainer, config, resourcesRoster);

			var playerViewModelFactory = gameContainer.Resolve<Model.IFactory<ViewModel.IPlayerViewModel>>();
			_playerController = playerViewModelFactory.CreateNew();
			//_playerController.SetViewRect(_rootTransorm);
		}

		private void Update() {
			_playerController.Update();
		}
	}
}
