﻿using Zenject;

namespace InfiniteSpace {
	public static class ZenjectInstallerHelper {

		public static DiContainer RegisterSingleton<TInterface, TImplementation>(this DiContainer container) where TImplementation : TInterface {
			container.Bind<TInterface>().To<TImplementation>().AsSingle();
			return container;
		}

		public static DiContainer RegisterType<TInterface, TImplementation>(this DiContainer container) where TImplementation : TInterface {
			container.Bind<TInterface>().To<TImplementation>().AsTransient();
			return container;
		}

		public static DiContainer RegisterInstance<T>(this DiContainer container, T instance) {
			container.Bind<T>().FromInstance(instance);
			return container;
		}

	}
}
