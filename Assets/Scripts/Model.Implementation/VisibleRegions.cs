﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class VisibleRegions : IVisibleRegions {
		//TODO: в дальнейшем можно будет сделать чистку регионов, что бы избежать утечки памяти в словаре
		private IFactory<IRegion> _regionFactory;
		private Config _config;
		private Dictionary<(uint x, uint y), IRegion> _regions
			= new Dictionary<(uint x, uint y), IRegion>();

		public IEnumerable<IRegionInfo> GetVisibleRegions(Rect frame) {
			var bitSize = _config.RegionBitSize;

			var regionSize = 1U << bitSize;

			var regionsRect = new Rect(
				frame.MinX >> bitSize,
				frame.MinY >> bitSize,
				frame.MaxX >> bitSize,
				frame.MaxY >> bitSize
			);

			var endX = ((regionsRect.MaxX << bitSize) + regionSize) >> bitSize;
			var endY = ((regionsRect.MaxY << bitSize) + regionSize) >> bitSize;
			//Конструкция rx = (rx * regionSize + regionSize) / regionSize; нужна для переполнения переменной
			for (var rx = regionsRect.MinX; rx != endX; rx = ((rx << bitSize) + regionSize) >> bitSize) {
				for (var ry = regionsRect.MinY; ry != endY; ry = ((ry << bitSize) + regionSize) >> bitSize) {
					IRegion region;
					if (!_regions.TryGetValue((rx, ry), out region)) {
						region = _regionFactory.CreateNew();
						_regions[(rx, ry)] = region;
						region.SetFullRegionRect(new Rect(
							rx << bitSize,
							ry << bitSize,
							(rx << bitSize) + regionSize - 1,
							(ry << bitSize) +  regionSize - 1
						));
					}

					if (rx != regionsRect.MinX && rx != regionsRect.MaxX
						&& ry != regionsRect.MinY && ry != regionsRect.MaxY) {
						region.SetIsFullVisible(true);
						region.SetActualRect(region.FullRegionRect);
					} else {
						region.SetActualRect(new Rect(
							rx == regionsRect.MinX
							? Math.Max(region.FullRegionRect.MinX, frame.MinX)
							: region.FullRegionRect.MinX,
							ry == regionsRect.MinY
							? Math.Max(region.FullRegionRect.MinY, frame.MinY)
							: region.FullRegionRect.MinY,
							rx == regionsRect.MaxX
							? Math.Min(region.FullRegionRect.MaxX, frame.MaxX)
							: region.FullRegionRect.MaxX,
							ry == regionsRect.MaxY
							? Math.Min(region.FullRegionRect.MaxY, frame.MaxY)
							: region.FullRegionRect.MaxY
						));
						region.SetIsFullVisible(region.ActualRect.Equals(region.FullRegionRect));
					}
					yield return region;
				}
			}
		}

		public VisibleRegions(Config config, IFactory<IRegion> regionFactory) {
			_regionFactory = regionFactory;
			_config = config;
		}
	}
}
