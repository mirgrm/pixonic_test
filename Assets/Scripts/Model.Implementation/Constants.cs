﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public static class Constants {

		public static (uint X, uint Y, uint Rating) ErrorPlanet = 
			(uint.MaxValue, uint.MaxValue, uint.MaxValue);

		public static (uint X, uint Y, uint Delta) EmtyPlanetInfo =
			(uint.MaxValue, uint.MaxValue, uint.MaxValue);

		public static uint ErrorDelta = uint.MaxValue;

	}
}
