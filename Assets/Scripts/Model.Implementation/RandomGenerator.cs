﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfiniteSpace.Model.Implementation {
	public class RandomGenerator : IRandomGenerator {
		public uint GetRandom(uint seed, uint maxValue) {
			// Нельзя инициализировать нулем, поэтому будем использовать другое значение
			if (seed == 0) seed = 123459876U;
			var hi = seed / 127773U;
			var lo = seed % 127773U;
			var x = (16807U * lo - 2836U * hi);// ^ (seed & maxValue);
			return (x % (maxValue));
		}
	}
}
