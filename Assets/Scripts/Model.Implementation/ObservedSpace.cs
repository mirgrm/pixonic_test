﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class ObservedSpace : IObservedSpace {

		private Rect _frame;
		private IVisibleRegions _visibleRegions;
		private IRegionsScanner _regionsScanner;
		private Config _config;
		public bool ScanWaiting => !_regionsScanner.Scanned;
		public IReadonlySortedPlanetList BestPlanets => _regionsScanner.BestPlanets;
		public Rect Frame {
			get => _frame;
			set  {
				if (_frame.MaxX != value.MaxX 
					|| _frame.MaxY != value.MaxY 
					|| _frame.MinX != value.MinX
					|| _frame.MinY != value.MinY) {
					OnFrameChanging(_frame, value);
					_frame = value;
				}
			}
		}
		public ObservedSpace(Config config, IVisibleRegions visibleRegions, 
			IRegionsScanner regionsScanner) {
			_config = config;
			_visibleRegions = visibleRegions;
			_regionsScanner = regionsScanner;
			Frame = config.StartFrame;
		}

		public void Update() {
			if (ScanWaiting) {
				_regionsScanner.Scan(_visibleRegions.GetVisibleRegions(Frame));
			}
		}

		private void OnFrameChanging(Rect oldFrame, Rect newFrame) {
			_regionsScanner.Scan(_visibleRegions.GetVisibleRegions(newFrame));
		}
	}
}
