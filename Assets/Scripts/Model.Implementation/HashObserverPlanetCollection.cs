﻿using System;
using System.Collections.Generic;

namespace InfiniteSpace.Model.Implementation {
	public class HashObserverPlanetCollection : IObserverPlanetCollection {
		private HashSet<(uint X, uint Y)> _set1 = new HashSet<(uint X, uint Y)>();
		private HashSet<(uint X, uint Y)> _set2 = new HashSet<(uint X, uint Y)>();

		public event Action<(uint X, uint Y)> AddingPlanet;
		public event Action<(uint X, uint Y)> DeletingPlanet;

		public HashObserverPlanetCollection() {

		}

		public void Update(IEnumerable<(uint X, uint Y)> planets) {
			foreach (var planet in planets) {
				_set2.Add(planet);
			}
			if (DeletingPlanet != null) {
				foreach (var planet in _set1) {
					if (!_set2.Contains(planet)) {
						DeletingPlanet(planet);
					}
				}
			}
			if (AddingPlanet != null) {
				foreach (var planet in _set2) {
					if (!_set1.Contains(planet)) {
						AddingPlanet(planet);
					}
				}
			}
			var temp = _set1;
			_set1 = _set2;
			_set2 = temp;
			_set2.Clear();
		}
	}
}
