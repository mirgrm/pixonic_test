﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class RegionsScanner : IRegionsScanner {
		private ISortedPlanetList _buffer;
		private Config _config;
		private IPlayer _player;

		public IReadonlySortedPlanetList BestPlanets => _buffer;

		public bool Scanned { get; private set; }

		public bool Scan(IEnumerable<IRegionInfo> regions) {
			TryAddPlanetsInBuffer(regions);
			return Scanned = RequestMorePlanets(regions);
		}

		bool RequestMorePlanets(IEnumerable<IRegionInfo> regions) {
			var completed = true;
			foreach (var region in regions) {
				var countFoundInRegion = region.VisiblePlanets.CountItemInArray;
				if (countFoundInRegion < _config.TopPlanetsCount) {
					if (countFoundInRegion == 0) {
						completed = false;
						region.RequestMorePlanets(1);
					} else {
						var lastPlanet = region.VisiblePlanets[countFoundInRegion - 1];
						if (lastPlanet.Delta!= Constants.ErrorDelta && _buffer.Contains(lastPlanet.X, lastPlanet.Y)) {
							completed = false;
							region.RequestMorePlanets(countFoundInRegion + 1);
						}
					}
				}
			}
			return completed;
		}

		uint CalcDeltaPoint(uint a, uint b) {
			if (a > uint.MaxValue - 10000 && b < 10000) return uint.MaxValue - a + b;
			if (b > uint.MaxValue - 10000 && a < 10000) return uint.MaxValue - b + a;
			return Math.Min(a - b, b - a);
		}

		uint CalcDelta((uint x, uint y, uint dRating) planet) {
			var dx = CalcDeltaPoint(_player.Point.X, planet.x);
			var dy = CalcDeltaPoint(_player.Point.Y, planet.y);
			return (planet.dRating << 16) + dx + dy;
		}

		(uint X, uint Y, uint Delta) PlanetConvert((uint X, uint Y, uint Delta) planet) {
			if (planet.Delta == Constants.ErrorDelta) return planet;
			return (planet.X, planet.Y, CalcDelta(planet));
		}

		void TryAddPlanetsInBuffer(IEnumerable<IRegionInfo> regions) {
			_buffer.CountItemInArray = 0;
			foreach (var region in regions) {
				var countFoundInRegion = region.VisiblePlanets.CountItemInArray;
				for (var i = 0U; i < region.VisiblePlanets.CountItemInArray; i++) {
					var planet = region.VisiblePlanets[i];
					_buffer.TryEnqueueAndIcnrement(PlanetConvert(planet));
				}
			}
		}

		public RegionsScanner(ISortedPlanetList buffer, Config config, IPlayer player) {
			_buffer = buffer;
			_config = config;
			_player = player;
		}
	}
}
