﻿using System.Threading.Tasks;

namespace InfiniteSpace.Model.Implementation {
	public class AsyncRegion : IRegion {
		private IPlanetFinder _fullPlanetFinder;
		private IPlanetFinder _actualPlanetFinder;
		private IPlayer _player;
		private Config _config;
		private volatile uint _requestedFull = 0;
		private volatile uint _requestedActual = 0;
		private volatile uint _needCountFullPlanets;
		private volatile uint _needCountActualPlanets;
		System.Threading.SemaphoreSlim _semaphore = new System.Threading.SemaphoreSlim(1);


		public Rect FullRegionRect { get; private set; }
		public Rect ActualRect { get; private set; }
		public bool IsFullVisible { get; private set; }

		public IReadonlySortedPlanetList VisiblePlanets => IsFullVisible
			? _fullPlanetFinder.PlanetList
			: _actualPlanetFinder.PlanetList;

		public void RequestMorePlanets(uint count) {
			if (count > _config.TopPlanetsCount) count = _config.TopPlanetsCount;
			if (IsFullVisible) {
				if (_needCountFullPlanets < count) {
					_needCountFullPlanets = count;
					Task.Run(RefreshFullRegion);
				}
			} else {
				if (_needCountActualPlanets < count) {
					_needCountActualPlanets = count;
					Task.Run(RefreshActualRegion);				
				}
			}
		}

		bool AllActualPlanetsInRect() {
			var planets = _actualPlanetFinder.PlanetList;
			var rect = ActualRect;
			for (var i = 0U; i < planets.CountItemInArray; i++) {
				var planet = planets[i];
				if (!rect.Contains(planet.X, planet.Y)) {
					return false;
				}
			}
			return true;
		}

		private async Task RefreshFullRegion() {
			try {
				await _semaphore.WaitAsync();
				if (_requestedFull < _needCountFullPlanets) {
					_requestedFull = _needCountFullPlanets;
					_fullPlanetFinder.SearchArrayBestPlanets(
						FullRegionRect, _player, _requestedFull);
				}
			} finally {
				_semaphore.Release();
			}
		}

		private async Task RefreshActualRegion() {
			try {
				await _semaphore.WaitAsync();
				if (_requestedActual < _needCountActualPlanets) {
					_requestedActual = _needCountActualPlanets;
					_actualPlanetFinder.SearchArrayBestPlanets(ActualRect, _player, _requestedActual);
				}
			} finally {
				_semaphore.Release();
			}
		}

		public void SetFullRegionRect(Rect rect) {
			FullRegionRect = rect;
			_needCountFullPlanets = 0;
			_requestedFull = 0;
		}

		public async void SetActualRect(Rect rect) {
			if (!ActualRect.Equals(rect)) {
				if (_needCountActualPlanets > 0) {
					var needUpdate = rect.Contains(ActualRect) || !AllActualPlanetsInRect();
					ActualRect = rect;
					_requestedActual = 0;
					await RefreshActualRegion();
				} else {
					ActualRect = rect;
				}
			}
		}

		public void SetIsFullVisible(bool full) {
			IsFullVisible = full;
		}

		public AsyncRegion(IPlanetFinder fullPlanetsFinder, IPlanetFinder actualPlanetFinder,
			IPlayer player, Config config) {
			_fullPlanetFinder = fullPlanetsFinder;
			_actualPlanetFinder = actualPlanetFinder;
			_player = player;
			_config = config;
		}
	}
}
