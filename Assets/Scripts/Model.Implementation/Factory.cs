﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class Factory<T> : IFactory<T> {
		private Func<T> _create;
		public T CreateNew() {
			return _create();
		}
		public Factory(Func<T> create) {
			_create = create;
		}
	}
}
