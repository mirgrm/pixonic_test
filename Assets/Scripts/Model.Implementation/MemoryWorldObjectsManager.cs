﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class MemoryWorldObjectsManager : IWorldObjectsManager {

		Dictionary<(uint, uint), uint> _dict = new Dictionary<(uint, uint), uint>();

		public uint GetPlanetRating(uint x, uint y) {
			return _dict[(x, y)];
		}

		public bool HasPlanet(uint x, uint y) {
			uint rating;
			return _dict.TryGetValue((x, y), out rating);
		}

		public MemoryWorldObjectsManager AddPlanet(uint x, uint y, uint rating) {
			_dict[(x, y)] = rating;
			return this;
		}

	}
}
