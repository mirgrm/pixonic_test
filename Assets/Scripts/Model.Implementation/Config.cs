﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class Config {
		public uint PlanetsProbabilityPercents = 30;
		public uint MaxPlanetRating = 10000;
		public uint TopPlanetsCount = 20;
		public Rect StartFrame = new Rect(0U, 0U, 1000U, 1000U);
		public int RegionBitSize = 7;
	}
}
