﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class WorldObjectsManager : IWorldObjectsManager {
		private IRandomGenerator _random;
		private Config _config;

		public uint GetPlanetRating(uint x, uint y) {
			var seed = (y << 16) ^ x;
			return _random.GetRandom(seed, _config.MaxPlanetRating);
		}

		public bool HasPlanet(uint x, uint y) {
			var seed = (x << 16) ^ y;
			var probability = _random.GetRandom(seed, 100);
			return probability < _config.PlanetsProbabilityPercents;
		}

		public WorldObjectsManager(IRandomGenerator randomGenerator, Config config) {
			_random = randomGenerator;
			_config = config;
		}
	}
}
