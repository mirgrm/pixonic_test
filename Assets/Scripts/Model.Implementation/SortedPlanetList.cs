﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;



namespace InfiniteSpace.Model.Implementation {
	public class SortedPlanetList : ISortedPlanetList {
		private (uint X, uint Y, uint Delta)[] _planets;

		public uint CountItemInArray { get; set; }

		public (uint X, uint Y, uint Delta) this[uint index] => 
			index>=CountItemInArray?Constants.EmtyPlanetInfo: _planets[index];

		public ISortedPlanetList ForceEnqueue((uint X, uint Y, uint Delta) planetInfo) {
			for (int i = 0; i < CountItemInArray-1; i++) {
				if (planetInfo.Delta < _planets[i].Delta) {
					var old = _planets[i];
					_planets[i] = planetInfo;
					planetInfo = old;
				}
			}
			_planets[CountItemInArray - 1] = planetInfo;
			return this;
		}

		public ISortedPlanetList TryEnqueue((uint X, uint Y, uint Delta) planetInfo) {
			if (planetInfo.Delta < _planets[CountItemInArray - 1].Delta)
				return ForceEnqueue(planetInfo);
			else return this;
		}

		public bool TryEnqueueAndIcnrement((uint X, uint Y, uint Delta) planetInfo) {
			if (CountItemInArray>0 && CountItemInArray<_planets.Length && planetInfo.Delta < _planets[CountItemInArray - 1].Delta) {
				CountItemInArray++;
				ForceEnqueue(planetInfo);
				return true;
			} else if (CountItemInArray < _planets.Length) {
				CountItemInArray++;
				_planets[CountItemInArray - 1] = planetInfo;
				return true;
			} else if (planetInfo.Delta < _planets[CountItemInArray - 1].Delta) {
				ForceEnqueue(planetInfo);
				return true;
			} else {
				return false;
			}
		}

		public bool Contains((uint X, uint Y, uint Delta) planetInfo) {
			if (MaxDelta >= planetInfo.Delta) {
				return Search(_planets, planetInfo, (int)CountItemInArray) >= 0;
			} else return false;
		}

		public bool Contains(uint x, uint y) {
			return SearchXY(_planets, x, y, (int)CountItemInArray) >= 0;
		}

		public int Search((uint X, uint Y, uint Delta)[] data, (uint X, uint Y, uint Delta) value, int count) {
			for (int i=0; i<count; i++) {
				var current = data[i];
				if (value.Delta < current.Delta) return -1;
				if (value == current) return i;
			}
			return -1;
		}

		public int SearchXY((uint X, uint Y, uint Delta)[] data, uint X, uint Y, int count) {
			for (int i = 0; i < count; i++) {
				var current = data[i];
				if (X == current.X && Y == current.Y) return i;
			}
			return -1;
		}

		public int BinarySearch((uint X, uint Y, uint Delta)[] data, (uint X, uint Y, uint Delta) value, int left, int right) {
			if (left > right) return -1;
			if (left == right) {
				if (data[left] == value)
					return left;
				else return -1;
			}
			while (true) {
				if (right - left == 1) {
					if (data[left] == value)
						return left;
					if (data[right] == value)
						return right;
					return -1;
				} else {
					var middle = left + (right - left) / 2;
					var comparisonResult = data[middle].Delta.CompareTo(value.Delta);
					if (comparisonResult == 0)
						return middle;
					if (comparisonResult < 0)
						left = middle;
					if (comparisonResult > 0)
						right = middle;
				}
			}
		}

		public IEnumerator<(uint X, uint Y)> GetEnumerator() {
			for (int i = 0; i < CountItemInArray; i++) {
				var current = _planets[i];
				yield return (current.X, current.Y);
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}

		public uint MaxDelta => CountItemInArray == 0 ? Constants.ErrorDelta : _planets[CountItemInArray - 1].Delta;

		public SortedPlanetList(Config config) {
			_planets = new (uint X, uint Y, uint Delta)[config.TopPlanetsCount];
			for (int i=0; i<config.TopPlanetsCount; i++) {
				_planets[i] = Constants.EmtyPlanetInfo;
			}
		}
	}
}
