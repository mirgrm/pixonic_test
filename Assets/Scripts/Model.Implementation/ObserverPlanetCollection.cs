﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class ObserverPlanetCollection : IObserverPlanetCollection {
		private ISortedPlanetList _set1;
		private ISortedPlanetList _set2;

		public event Action<(uint X, uint Y)> AddingPlanet;
		public event Action<(uint X, uint Y)> DeletingPlanet;

		public ObserverPlanetCollection(ISortedPlanetList set1, ISortedPlanetList set2) {
			_set1 = set1;
			_set2 = set2;
		}

		public void Update(IEnumerable<(uint X, uint Y)> planets) {
			foreach (var planet in planets) {
				_set2.TryEnqueueAndIcnrement((planet.X, planet.Y, 0));
			}
			if (DeletingPlanet != null) {
				for (var i = 0U; i < _set1.CountItemInArray; i++) {
					var planet = _set1[i];
					if (!_set2.Contains(planet.X, planet.Y)) {
						DeletingPlanet((planet.X, planet.Y));
					}
				}
			}
			if (AddingPlanet != null) {
				for (uint i = 0; i < _set2.CountItemInArray; i++) {
					var planet = _set2[i];
					if (!_set1.Contains(planet.X, planet.Y)) {
						AddingPlanet((planet.X, planet.Y));
					}
				}
			}
			var temp = _set1;
			_set1 = _set2;
			_set2 = temp;
			_set2.CountItemInArray = 0;
		}
	}
}
