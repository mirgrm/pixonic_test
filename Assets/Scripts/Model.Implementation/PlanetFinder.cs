﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class PlanetFinder : IPlanetFinder {
		private IWorldObjectsManager _worldObjectsManager;
		private ISortedPlanetList _sortetList;

		public IReadonlySortedPlanetList PlanetList => _sortetList;

		public (uint X, uint Y, uint Delta) SearchBestPlanet(Rect rect, IPlayer player, uint place) {
			if (place == 0) return SearchBestPlanet(rect, player);
			return SearchArrayBestPlanets(rect, player, place+1)[place];
		}

		public IReadonlySortedPlanetList SearchArrayBestPlanets(Rect rect, IPlayer player, uint count) {
			var list = _sortetList; //Используем постоянную коллекцию, что бы не мучить GC
			var lowDeltaInArray = Constants.ErrorDelta;
			var countItemInArray = 0U;
			for (var x = rect.MinX; x != rect.MaxX+1; x++) {
				for (var y = rect.MinY; y != rect.MaxY+1; y++) {
					if (_worldObjectsManager.HasPlanet(x, y)) {
						var planetRating = _worldObjectsManager.GetPlanetRating(x, y);
						var delta = player.CalcDelta(x, y, planetRating);
						if (countItemInArray < count || delta < lowDeltaInArray) {
							if (countItemInArray < count) {
								countItemInArray++;
								list.CountItemInArray = countItemInArray;
							}
							list.ForceEnqueue((x,y,delta));
							lowDeltaInArray = list[countItemInArray - 1].Delta;
						}
					}
				}
			}
			while (list.CountItemInArray < count) {
				list.TryEnqueueAndIcnrement(Constants.ErrorPlanet);
			}
			return list;
		}

		public (uint x, uint y, uint rating) SearchBestPlanet(Rect rect, IPlayer player) {
			var bestPlanet = (X: uint.MaxValue, Y: uint.MaxValue, Delta: uint.MaxValue);
			for (var x = rect.MinX; x != rect.MaxX+1; x++) {
				for (var y = rect.MinY; y != rect.MaxY+1; y++) {
					if (_worldObjectsManager.HasPlanet(x, y)) {
						var planetRating = _worldObjectsManager.GetPlanetRating(x, y);
						var delta = player.CalcDelta(x, y, planetRating);
						if (bestPlanet.Delta > delta) {
							bestPlanet = (x, y, delta);
						}
					}
				}
			}
			return bestPlanet;
		}


		public PlanetFinder(IWorldObjectsManager worldObjectsManager, ISortedPlanetList buffer) {
			_worldObjectsManager = worldObjectsManager;
			_sortetList = buffer;
		}
	}
}
