﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model.Implementation {
	public class Player : IPlayer {

		public uint Rating { get; }

		public Point Point { get; set; }

		public Player(Config config) {
			Rating = (uint)new Random().Next((int)config.MaxPlanetRating);
		}

		public Player(uint rating) {
			Rating = rating;
		}

		public uint CalcDelta(uint x, uint y, uint planetRating) {
			var dRating = Math.Min(Rating - planetRating, planetRating - Rating);
			return dRating;
		}

	}
}
