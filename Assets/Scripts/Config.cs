﻿namespace InfiniteSpace {
	public class GameConfig  {
		public uint MinScale = 2;
		public uint ChangeModeScale = 5;
		public uint MaxScale = 5000;
		public Model.Implementation.Config ModelConfig 
			= new Model.Implementation.Config();
	}
}
