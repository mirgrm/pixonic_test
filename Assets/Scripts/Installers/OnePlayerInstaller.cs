﻿using InfiniteSpace.Model;
using InfiniteSpace.Model.Implementation;
using UnityEngine;
using Zenject;

namespace InfiniteSpace {
	public class OnePlayerInstaller : Installer<GameConfig, ResourcesRoster, OnePlayerInstaller> {
		private GameConfig _config;
		private ResourcesRoster _resourcesRoster;

		public OnePlayerInstaller(GameConfig config, ResourcesRoster resourcesRoster) {
			_config = config;
			_resourcesRoster = resourcesRoster;
		}

		public override void InstallBindings() {
			BindModel();
			BindView();
			BindViewModel();
		}

		void BindModel() {
			Container
				.RegisterInstance<Config>(_config.ModelConfig)
				.RegisterSingleton<IRandomGenerator, RandomGenerator>()
				.RegisterType<IWorldObjectsManager, WorldObjectsManager>()
				.RegisterType<ISortedPlanetList, SortedPlanetList>()
				.RegisterType<IPlanetFinder, PlanetFinder>()
				.RegisterType<IRegionsScanner, RegionsScanner>()
				.RegisterType<IVisibleRegions, VisibleRegions>()
				#if UNITY_WEBGL
				.RegisterType<IRegion, Region>()
				#else
				.RegisterType<IRegion, AsyncRegion>()
				#endif
				.RegisterSingleton<IPlayer, Player>()
				.RegisterSingleton<IObservedSpace, ObservedSpace>()
				.RegisterType<IObserverPlanetCollection, HashObserverPlanetCollection>()
				.RegisterInstance<Model.IFactory<IRegion>>(
					new Model.Implementation.Factory<IRegion>(() =>
						Container.Resolve<IRegion>()));
		}

		void BindViewModel() {
			Container
				.RegisterInstance<GameConfig>(_config)
				.RegisterType<ViewModel.IPlayerViewModel, ViewModel.PlayerViewModel>()
				.RegisterType<ViewModel.IPlanetsPool, ViewModel.PlanetsPool>()
				.RegisterType<ViewModel.IPlayerInputController, ViewModel.PlayerInputController>()
				.RegisterType<ViewModel.IPlayerShip, ViewModel.PlayerShip>()
				.RegisterSingleton<ViewModel.IViewRoot, ViewModel.ViewRoot>()
				.RegisterSingleton<ViewModel.ISimplePlanetsViewer, ViewModel.SimplePlanetsViewer>();
		}

		void BindView() {
			Container
				.Bind<View.IViewFactory<View.SpaceShip>>()
				.FromInstance(new PrefabFactory<View.SpaceShip>(Container, _resourcesRoster.SpaceShip));
			Container
				.Bind<View.IViewFactory<View.Planet>>()
				.FromInstance(new PrefabFactory<View.Planet>(Container, _resourcesRoster.Planet));
			Container
				.Bind<View.IViewFactory<View.ViewRect>>()
				.FromInstance(new PrefabFactory<View.ViewRect>(Container, _resourcesRoster.ViewRect));
		}

		private class PrefabFactory<T> : View.IViewFactory<T> where T : MonoBehaviour {
			private DiContainer _container;
			private T _prefab;
			public PrefabFactory(DiContainer container, T prefab) {
				_container = container;
				_prefab = prefab;
			}
			public T Instantiate() {
				return _container.InstantiatePrefabForComponent<T>(_prefab);
			}
			public T Instantiate(Transform parentTransform) {
				return _container.InstantiatePrefabForComponent<T>(_prefab, parentTransform);
			}
			public T Instantiate(Vector3 poistion, Quaternion rotation, Transform parentTransform) {
				return _container.InstantiatePrefabForComponent<T>(_prefab, poistion, rotation, parentTransform);
			}
		}
	}


}
