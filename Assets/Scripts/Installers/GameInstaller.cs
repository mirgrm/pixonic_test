﻿using InfiniteSpace.ViewModel;
using InfiniteSpace.Model;
using InfiniteSpace.Model.Implementation;
using UnityEngine;
using Zenject;

namespace InfiniteSpace {
	public class GameInstaller : Installer<GameConfig, ResourcesRoster, GameInstaller> {
		private GameConfig _config;
		private ResourcesRoster _resourcesRoster;

		public GameInstaller(GameConfig config, ResourcesRoster resourcesRoster) {
			_config = config;
			_resourcesRoster = resourcesRoster;
		}

		public override void InstallBindings() {
			Container
				.RegisterInstance<Model.IFactory<IPlayerViewModel>>(
					new Model.Implementation.Factory<IPlayerViewModel>(() => {
						var onePlayerContainer = Container.CreateSubContainer();
						OnePlayerInstaller.Install(onePlayerContainer, _config, _resourcesRoster);
						return onePlayerContainer.Resolve<IPlayerViewModel>();
					}	
				));			
		}
	}
}
