﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	/// <summary> Наблюдаемый космос </summary>
	public interface IObservedSpace {

		/// <summary> Идет ли сейчас процесс сканирования </summary>
		bool ScanWaiting { get; }

		/// <summary> Список лучших планет </summary>
		IReadonlySortedPlanetList BestPlanets { get; }

		/// <summary> Границы видимой области космоса </summary>
		Rect Frame { get; set; }

		void Update();
	}
}
