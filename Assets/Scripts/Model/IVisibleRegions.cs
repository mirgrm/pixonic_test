﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public interface IVisibleRegions {
		/// <summary> Возвращает коллекцию регионов в обозреваемом пространстве </summary>
		IEnumerable<IRegionInfo> GetVisibleRegions(Rect frame);
	}
}
