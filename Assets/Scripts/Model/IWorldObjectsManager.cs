﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {

	/// <summary>
	/// Позволяет получить информацию о планетах по координатам
	/// </summary>
	public interface IWorldObjectsManager {

		bool HasPlanet(uint x, uint y);

		uint GetPlanetRating(uint x, uint y); 

	}
}
