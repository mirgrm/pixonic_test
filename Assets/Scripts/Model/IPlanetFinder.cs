﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {

	/// <summary> Ищет лучшую планету в регионе </summary>
	public interface IPlanetFinder {

		/// <summary>
		/// Ищет лучшую планету на определенном месте.
		/// То есть если место третье, то найдет третью самую лушую планету
		/// </summary>
		/// <param name="rect"> Область поиска </param>
		/// <param name="place"> Место планеты в рейтинге лучших в данной области начиная с 0 </param>
		/// <returns></returns>
		(uint X, uint Y, uint Delta) SearchBestPlanet(Rect rect, IPlayer player, uint place);

		/// <summary> Ищет список из count лучших планет </summary>
		IReadonlySortedPlanetList SearchArrayBestPlanets(Rect rect, IPlayer player, uint count);

		IReadonlySortedPlanetList PlanetList { get; }

	}
}
