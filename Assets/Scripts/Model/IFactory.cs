﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public interface IFactory<T> {
		T CreateNew();
	}
}
