﻿using System;

namespace InfiniteSpace.Model {

	/// <summary>
	/// Регион - это квадратная область пространства из нескольких ячеек, которая четко привязана к координатам
	/// </summary>
	public interface IRegion : IRegionInfo {
		/// <summary> Прямоугольник, определяющий размер региона </summary>
		void SetFullRegionRect(Rect rect);
		/// <summary> Видимая зона, если наблюдается на весь регион </summary>
		void SetActualRect(Rect rect);
		/// <summary> В регионе наблюдается вся территория? </summary>
		void SetIsFullVisible(bool full);
	}
}
