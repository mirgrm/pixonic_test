﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public struct Rect {
		public readonly uint MinX;
		public readonly uint MaxX;
		public readonly uint MinY;
		public readonly uint MaxY;

		public Rect(uint minX, uint minY, uint maxX, uint maxY) {
			MinX = minX;
			MaxX = maxX;
			MinY = minY;
			MaxY = maxY;
		}

		public Rect(uint maxX, uint maxY) {
			MinX = 0;
			MaxX = maxX;
			MinY = 0;
			MaxY = maxY;
		}

		public uint SizeX => MaxX - MinX + 1U;
		public uint SizeY => MaxY - MinY + 1U;

		/// <summary> Данный метод не учитывает возможное переполнение переменной, когда min > max </summary>
		public bool Contains(uint X, uint Y) {
			var containsX = MinX <= X && X <= MaxX;
			var containsY = MinY <= Y && Y <= MaxY;
			return containsX && containsY;
		}

		public bool Contains(Rect small) {
			return Contains(small.MinX, small.MinY) 
				&& Contains(small.MinX, small.MaxY)
				&& Contains(small.MaxX, small.MinY)
				&& Contains(small.MaxY, small.MaxX);
		}

		public static Rect operator +(Rect rect, Point p) {
			return new Rect(rect.MinX + p.X, rect.MinY + p.Y, rect.MaxX + p.X, rect.MaxY + p.Y);
		}

		public static Rect operator -(Rect rect, Point p) {
			return new Rect(rect.MinX - p.X, rect.MinY - p.Y, rect.MaxX - p.X, rect.MaxY - p.Y);
		}

		public static Rect operator +(Point p, Rect rect) {
			return new Rect(rect.MinX + p.X, rect.MinY + p.Y, rect.MaxX + p.X, rect.MaxY + p.Y);
		}

		public static Rect operator -(Point p, Rect rect) {
			return new Rect(rect.MinX - p.X, rect.MinY - p.Y, rect.MaxX - p.X, rect.MaxY - p.Y);
		}
	}

	public struct Point {
		public readonly uint X;
		public readonly uint Y;
		public Point(uint x, uint y) {
			X = x;
			Y = y;
		}

		public static Point operator +(Point a, Point b) {
			return new Point(a.X + b.X, a.Y + b.Y);
		}

		public static Point operator -(Point a, Point b) {
			return new Point(a.X - b.X, a.Y - b.Y);
		}
	}
}
