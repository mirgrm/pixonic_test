﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public interface ISortedPlanetList : IReadonlySortedPlanetList {
		/// <summary>
		/// Принудительное добавление. 
		/// Если все содержимые элементы лучше, то заменяется последний элемент
		/// </summary>
		ISortedPlanetList ForceEnqueue((uint X, uint Y, uint Delta) planetInfo);

		/// <summary>
		/// Перед добавление идет проверка, что новый элемент не хуже последнего
		/// </summary>
		ISortedPlanetList TryEnqueue((uint X, uint Y, uint Delta) planetInfo);

		/// <summary>
		/// Пытается добавить планету, если планета хуже того что уже есть, 
		/// то пытается добавить в конец и увеличить Count, но в пределах того значения,
		/// что уже указано в конфиге
		/// </summary>
		/// <returns> Удалось ли добавить </returns>
		bool TryEnqueueAndIcnrement((uint X, uint Y, uint Delta) planetInfo);
	}

	public interface IReadonlySortedPlanetList : IEnumerable<(uint X, uint Y)> {
		/// <summary> Количество значимых элементов в коллекции </summary>
		uint CountItemInArray { get; set; }
		/// <summary> Возвращает информацию по планете по приоритетному индексу </summary>
		(uint X, uint Y, uint Delta) this[uint index] { get; }
		/// <summary> определяет, есть ли planet в коллекции </summary>
		bool Contains((uint X, uint Y, uint Delta) planet);
		/// <summary> определяет, есть ли планета с координатами x и y в коллекции </summary>
		bool Contains(uint x, uint y);
		/// <summary> Максимальное значение Delta среди всех планет в списке </summary>
		uint MaxDelta { get; }
	}
}
