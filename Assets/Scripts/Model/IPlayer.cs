﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public interface IPlayer {
		uint Rating { get; }
		Point Point { get; set; }
		uint CalcDelta(uint x, uint y, uint planetRating);
	}
}
