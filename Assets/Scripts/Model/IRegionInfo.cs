﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {

	/// <summary>
	/// Информация о регионе для IRegionsScanner
	/// </summary>
	public interface IRegionInfo {
		/// <summary> Список текущих видимых планет </summary>
		IReadonlySortedPlanetList VisiblePlanets { get; }

		/// <summary> Задает необходимость найти еще планеты </summary>
		void RequestMorePlanets(uint count);

		/// <summary> Прямоугольник, определяющий размер региона </summary>
		Rect FullRegionRect { get;  }
		/// <summary> Видимая зона, если наблюдается на весь регион </summary>
		Rect ActualRect { get;  }
		/// <summary> В регионе наблюдается вся территория? </summary>
		bool IsFullVisible { get;  }
	}
}
