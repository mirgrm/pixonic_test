﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {

	/// <summary>
	/// Данный класс просматривает все регионы
	/// в поиске лучших планет для отображения
	/// </summary>
	public interface IRegionsScanner {
		/// <summary> Результаты промежуточного или конечного сканирования </summary>
		IReadonlySortedPlanetList BestPlanets { get; }
		/// <summary> Завершено ли сканирование, или требуется повторное </summary>
		bool Scanned { get; }
		/// <summary> Ищет лучшие планеты в коллекции регионов </summary>
		/// <returns>Возвращает истину, если сканирование завершено </returns>
		bool Scan(IEnumerable<IRegionInfo> regions);
	}
}
