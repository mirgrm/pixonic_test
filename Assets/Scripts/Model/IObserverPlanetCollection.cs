﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public interface IObserverPlanetCollection {
		void Update(IEnumerable<(uint X, uint Y)> planets);
		event Action<(uint X, uint Y)> AddingPlanet;
		event Action<(uint X, uint Y)> DeletingPlanet;
	}
}
