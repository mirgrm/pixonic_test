﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfiniteSpace.Model {
	public interface IRandomGenerator {

		uint GetRandom(uint seed, uint maxValue);
	}
}
